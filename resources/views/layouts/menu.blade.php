<li class="{{ Request::is('genres*') ? 'active' : '' }}">
    <a href="{{ route('genres.index') }}"><i class="fa fa-edit"></i><span>Genres</span></a>
</li>

<li class="{{ Request::is('movies*') ? 'active' : '' }}">
    <a href="{{ route('movies.index') }}"><i class="fa fa-edit"></i><span>Movies</span></a>
</li>

<li class="{{ Request::is('episodes*') ? 'active' : '' }}">
    <a href="{{ route('episodes.index') }}"><i class="fa fa-edit"></i><span>Episodes</span></a>
</li>

