<!-- Genres Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('genres_id', 'Genre:') !!}
    {!! Form::select('genres_id',$genre, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::select('rating', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('movies.index') }}" class="btn btn-default">Cancel</a>
</div>
