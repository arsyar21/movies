<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $movie->title }}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{{ $movie->photo }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $movie->description }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $movie->rating }}</p>
</div>

<!-- Genres Id Field -->
<div class="form-group">
    {!! Form::label('genres_id', 'Genres Id:') !!}
    <p>{{ $movie->genres_id }}</p>
</div>

