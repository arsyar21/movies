<div class="table-responsive">
    <table class="table table-bordered" id="movies-table">
        <thead>
            <tr>
        <th>Genre</th>
        <th>Title</th>
        <th>Description</th>
        <th>Rating</th>
        <th>Picture</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($movies as $movie)
            <tr>
            <td>{{ $movie->genres->name }}</td>
            <td>{{ $movie->title }}</td>
            <td>{{ $movie->description }}</td>
            <td>{{ $movie->rating }}</td>
            <td><img src="{{asset($movie->photo)}}" style="width:100px;height:100px;padding:10px;background-color:#f2f2f2;" alt=""></td>
            
                <td>
                    {!! Form::open(['route' => ['movies.destroy', $movie->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('movies.show', [$movie->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('movies.edit', [$movie->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
