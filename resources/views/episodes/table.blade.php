<div class="table-responsive">
    <table class="table table-bordered" id="episodes-table">
        <thead>
            <tr>
        <th>Movies</th>
        <th>Episode</th>
        <th>Title</th>
        
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($episodes as $episode)
            <tr>
                <td>{{ $episode->movies->title }}</td>
                <td>{{ $episode->episode }}</td>
                <td>{{ $episode->title }}</td>
            
                <td>
                    {!! Form::open(['route' => ['episodes.destroy', $episode->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('episodes.show', [$episode->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('episodes.edit', [$episode->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
