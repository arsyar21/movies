<!-- Movies Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('movies_id', 'Movies Id:') !!}
    {!! Form::select('movies_id',$movie, null, ['class' => 'form-control']) !!}
</div>

<!-- Episode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('episode', 'Episode:') !!}
    {!! Form::text('episode', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 225,'maxlength' => 225]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('episodes.index') }}" class="btn btn-default">Cancel</a>
</div>
