<!-- Episode Field -->
<div class="form-group">
    {!! Form::label('episode', 'Episode:') !!}
    <p>{{ $episode->episode }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $episode->title }}</p>
</div>

<!-- Movies Id Field -->
<div class="form-group">
    {!! Form::label('movies_id', 'Movies Id:') !!}
    <p>{{ $episode->movies_id }}</p>
</div>

