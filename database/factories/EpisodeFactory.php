<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Episode;
use Faker\Generator as Faker;

$factory->define(Episode::class, function (Faker $faker) {

    return [
        'episode' => $faker->word,
        'title' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'movies_id' => $faker->randomDigitNotNull
    ];
});
