<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Episode
 * @package App\Models
 * @version November 6, 2020, 10:41 am UTC
 *
 * @property \App\Models\Movie $movies
 * @property string $episode
 * @property string $title
 * @property integer $movies_id
 */
class Episode extends Model
{
    use SoftDeletes;

    public $table = 'episodes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'episode',
        'title',
        'movies_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'episode' => 'string',
        'title' => 'string',
        'movies_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'episode' => 'nullable|string|max:225',
        'title' => 'nullable|string|max:225',
        'created_at' => 'nullable',
        'deleted_at' => 'nullable',
        'updated_at' => 'nullable',
        'movies_id' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function movies()
    {
        return $this->belongsTo(\App\Models\Movie::class, 'movies_id');
    }
}
