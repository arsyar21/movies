<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Movie
 * @package App\Models
 * @version November 6, 2020, 10:41 am UTC
 *
 * @property \App\Models\Genre $genres
 * @property \Illuminate\Database\Eloquent\Collection $episodes
 * @property string $title
 * @property string $photo
 * @property string $description
 * @property string $rating
 * @property integer $genres_id
 */
class Movie extends Model
{
    use SoftDeletes;

    public $table = 'movies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'photo',
        'description',
        'rating',
        'genres_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'photo' => 'string',
        'description' => 'string',
        'rating' => 'string',
        'genres_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:225',
        'created_at' => 'nullable',
        'deleted_at' => 'nullable',
        'updated_at' => 'nullable',
        'description' => 'nullable|string|max:225',
        'rating' => 'nullable|string|max:225',
        'genres_id' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function genres()
    {
        return $this->belongsTo(\App\Models\Genre::class, 'genres_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function episodes()
    {
        return $this->hasMany(\App\Models\Episode::class, 'movies_id');
    }
}
