<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Genre
 * @package App\Models
 * @version November 6, 2020, 10:41 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $movies
 * @property string $name
 */
class Genre extends Model
{
    use SoftDeletes;

    public $table = 'genres';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable|string|max:225',
        'created_at' => 'nullable',
        'deleted_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function movies()
    {
        return $this->hasMany(\App\Models\Movie::class, 'genres_id');
    }
}
