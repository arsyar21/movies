<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Repositories\BaseRepository;

/**
 * Class MovieRepository
 * @package App\Repositories
 * @version November 6, 2020, 10:41 am UTC
*/

class MovieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'photo',
        'description',
        'rating',
        'genres_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Movie::class;
    }
}
