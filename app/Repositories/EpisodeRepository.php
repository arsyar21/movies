<?php

namespace App\Repositories;

use App\Models\Episode;
use App\Repositories\BaseRepository;

/**
 * Class EpisodeRepository
 * @package App\Repositories
 * @version November 6, 2020, 10:41 am UTC
*/

class EpisodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'episode',
        'title',
        'movies_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Episode::class;
    }
}
