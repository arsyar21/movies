<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMovieRequest;
use App\Http\Requests\UpdateMovieRequest;
use App\Repositories\MovieRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Genre;
use DB;

class MovieController extends AppBaseController
{
    /** @var  MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepo)
    {
        $this->movieRepository = $movieRepo;
    }

    /**
     * Display a listing of the Movie.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $movies = $this->movieRepository->all();

        return view('movies.index')
            ->with('movies', $movies);
    }

    /**
     * Show the form for creating a new Movie.
     *
     * @return Response
     */
    public function create()
    {
        $genre = Genre::pluck('name','id');
        return view('movies.create',compact('genre'));
    }

    /**
     * Store a newly created Movie in storage.
     *
     * @param CreateMovieRequest $request
     *
     * @return Response
     */
    public function store(CreateMovieRequest $request)
    {
        $this->validate($request, [
            'photo'  => 'required|max:1046'
        ]);
         $input = $request->except('photo');
         try{
            DB::beginTransaction();
            $movie = $this->movieRepository->create($input);
            if( $request->hasFile('photo')){
                $file = $request->file('photo');
                $filename = $movie->name.'.'.$file->getClientOriginalExtension();
                $path=$request->photo->storeAs('public/movie', $filename,'local');
                $movie->photo='storage'.substr($path,strpos($path,'/'));
            }
            $movie->save();
            DB::commit();
        Flash::success('movie saved successfully.');
            }catch (Exception $e){
                DB::rollback();
                Flash::error('Data gagal ditambah');
            }
        return redirect(route('movies.index'));
    }

    /**
     * Display the specified Movie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error('Movie not found');

            return redirect(route('movies.index'));
        }

        return view('movies.show')->with('movie', $movie);
    }

    /**
     * Show the form for editing the specified Movie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $movie = $this->movieRepository->find($id);
        $genre = Genre::pluck('name','id');

        if (empty($movie)) {
            Flash::error('Movie not found');

            return redirect(route('movies.index'));
        }

        return view('movies.edit',compact('movie','genre'));
    }

    /**
     * Update the specified Movie in storage.
     *
     * @param int $id
     * @param UpdateMovieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovieRequest $request)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error('Movie not found');

            return redirect(route('movies.index'));
        }

        try{
            DB::beginTransaction();
            $movie = $this->movieRepository->update($request->except('photo'), $id);
            if( $request->hasFile('photo')){
                $file = $request->file('photo');
                $filename = $movie->name.'.'.$file->getClientOriginalExtension();
                $path=$request->photo->storeAs('public/movie', $filename,'local');
                $movie->photo='storage'.substr($path,strpos($path,'/'));
                $movie->save();
            }
            DB::commit();
        Flash::success('movie saved successfully.');
            }catch (Exception $e){
                DB::rollback();
                Flash::error('Data gagal ditambah');
            }

        return redirect(route('movies.index'));
    }

    /**
     * Remove the specified Movie from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error('Movie not found');

            return redirect(route('movies.index'));
        }

        $this->movieRepository->delete($id);

        Flash::success('Movie deleted successfully.');

        return redirect(route('movies.index'));
    }
}
