<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEpisodeRequest;
use App\Http\Requests\UpdateEpisodeRequest;
use App\Repositories\EpisodeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Movie;

class EpisodeController extends AppBaseController
{
    /** @var  EpisodeRepository */
    private $episodeRepository;

    public function __construct(EpisodeRepository $episodeRepo)
    {
        $this->episodeRepository = $episodeRepo;
    }

    /**
     * Display a listing of the Episode.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $episodes = $this->episodeRepository->all();

        return view('episodes.index')
            ->with('episodes', $episodes);
    }

    /**
     * Show the form for creating a new Episode.
     *
     * @return Response
     */
    public function create()
    {
        $movie = Movie::pluck('title','id');
        return view('episodes.create',compact('movie'));
    }

    /**
     * Store a newly created Episode in storage.
     *
     * @param CreateEpisodeRequest $request
     *
     * @return Response
     */
    public function store(CreateEpisodeRequest $request)
    {
        $input = $request->all();

        $episode = $this->episodeRepository->create($input);

        Flash::success('Episode saved successfully.');

        return redirect(route('episodes.index'));
    }

    /**
     * Display the specified Episode.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $episode = $this->episodeRepository->find($id);

        if (empty($episode)) {
            Flash::error('Episode not found');

            return redirect(route('episodes.index'));
        }

        return view('episodes.show')->with('episode', $episode);
    }

    /**
     * Show the form for editing the specified Episode.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $episode = $this->episodeRepository->find($id);
        $movie = Movie::pluck('title','id');

        if (empty($episode)) {
            Flash::error('Episode not found');

            return redirect(route('episodes.index'));
        }

        return view('episodes.edit',compact('episode','movie'));
    }

    /**
     * Update the specified Episode in storage.
     *
     * @param int $id
     * @param UpdateEpisodeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEpisodeRequest $request)
    {
        $episode = $this->episodeRepository->find($id);

        if (empty($episode)) {
            Flash::error('Episode not found');

            return redirect(route('episodes.index'));
        }

        $episode = $this->episodeRepository->update($request->all(), $id);

        Flash::success('Episode updated successfully.');

        return redirect(route('episodes.index'));
    }

    /**
     * Remove the specified Episode from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $episode = $this->episodeRepository->find($id);

        if (empty($episode)) {
            Flash::error('Episode not found');

            return redirect(route('episodes.index'));
        }

        $this->episodeRepository->delete($id);

        Flash::success('Episode deleted successfully.');

        return redirect(route('episodes.index'));
    }
}
